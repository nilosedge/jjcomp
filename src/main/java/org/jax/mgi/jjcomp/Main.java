package org.jax.mgi.jjcomp;

import java.net.URL;

import org.apache.commons.io.IOUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flipkart.zjsonpatch.JsonDiff;

public class Main {

	public static void main(String[] args) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();

		URL url1 = new URL("http://localhost:9200/site_index/gene/MGI:3580234");
		URL url2 = new URL("http://dev.alliancegenome.org/es/site_index/gene/MGI:3580234");
		
		JsonNode json1 = mapper.readTree(IOUtils.toString(url1.openStream()));
		JsonNode json2 = mapper.readTree(IOUtils.toString(url2.openStream()));
		
		
		JsonNode patch = JsonDiff.asJson(json1, json2);
		System.out.println(patch);
	}

}
